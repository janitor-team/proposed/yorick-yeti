Author: Thibaut Paumard <thibaut@debian.org>
Date: 2016/11/30
Description: configure makefiles
 Configuring the package modifies the various Makefiles (usual for
 Yorick packages). Saving these changes in a patch which is the first
 in the series makes it easier to manage the _useful_ patches and
 avoid auto-generating a "debian-changes..." patch.
Forwarded: not-needed

--- /dev/null
+++ b/Makefile
@@ -0,0 +1,109 @@
+#
+# Makefile -
+#
+# Top-level Makefile for Yeti package and plugins.
+#
+#------------------------------------------------------------------------------
+
+# These values filled in by: yorick -batch make.i
+Y_MAKEDIR=/usr/lib/yorick
+Y_EXE=/usr/lib/yorick/bin/yorick
+Y_EXE_PKGS=
+Y_EXE_HOME=/usr/lib/yorick
+Y_EXE_SITE=/usr/lib/yorick
+Y_HOME_PKG=
+
+#---------------------------------------------------------- configuration flags
+
+# These values filled in by: yorick -batch config.i
+YETI_PKGS = core regex
+YETI_VERSION_MAJOR = 6
+YETI_VERSION_MINOR = 4
+YETI_VERSION_MICRO = 0
+YETI_VERSION_SUFFIX = ""
+YORICK_VERSION_MAJOR = 2
+YORICK_VERSION_MINOR = 2
+YORICK_VERSION_MICRO = 3
+YORICK_VERSION_SUFFIX = ""
+
+SUBDIRS = $(YETI_PKGS) doc
+
+TMPDIR=/tmp
+
+DISTRIB_PKGS = core fftw regex tiff
+DISTRIB_DIRS = doc $(DISTRIB_PKGS)
+DISTRIB_FILES = AUTHORS LICENSE.md NEWS README.md VERSION \
+	Makefile Makefile.in configure config.h.in config.i
+
+#------------------------------------------------------------------------------
+
+default: all
+
+all: check
+	@for dir in $(SUBDIRS); do \
+	  (cd $$dir; make); \
+	done
+
+check:
+	@if test -z "$(YETI_PKGS)" -o ! -r config.h ; then \
+	  echo "***************************************************************"; \
+	  echo "  Before building Yeti, you must run the configuration script."; \
+	  echo "  This is achieved by a command like:"; \
+	  echo "      yorick -batch ./config.i [...]"; \
+	  echo ""; \
+	  echo "  See README.md file for detailled instructions.  For a summary"; \
+	  echo "  of configuration options, you can also try:"; \
+	  echo "      yorick -batch ./config.i --help"; \
+	  echo "***************************************************************"; \
+	  false; \
+	else \
+	  true; \
+	fi
+
+install: check
+	@for dir in $(SUBDIRS); do \
+	  (cd $$dir; make $@); \
+	done
+#	@for src in AUTHORS COPYING NEWS README.md LICENSE.md VERSION; do \
+#	  dst=$(Y_EXE_SITE)/doc/$$src.yeti; \
+#	  cp -pf $$src $$dst; \
+#	  chmod 644 $$dst; \
+#	done
+
+clean: check
+	rm -f *~ *.tmp
+	@for dir in $(SUBDIRS); do \
+	  (cd $$dir; make $@); \
+	done
+
+distclean: clean
+	rm -f config.h Makefile
+
+distrib:
+	@version=`cat VERSION`; \
+	target="yeti-$${version}"; \
+	tmpdir="$(TMPDIR)/$${target}"; \
+	archive="$${tmpdir}.tar.bz2"; \
+	if test -d "$${tmpdir}"; then \
+	  echo "directory $${tmpdir} already exists"; \
+	  false; \
+	else \
+	  olddir=`pwd`; \
+	  mkdir -p "$${tmpdir}"; \
+	  for file in $(DISTRIB_FILES); do \
+	    cp -a "$${file}" "$${tmpdir}/$${file}"; \
+	  done; \
+	  for dir in $(DISTRIB_DIRS); do \
+	    cp -a "$${dir}" "$${tmpdir}/$${dir}"; \
+	  done; \
+	  cd "$${tmpdir}"; \
+	  touch config.h; \
+	  make clean YETI_PKGS="$(DISTRIB_PKGS)"; \
+	  rm -f config.h; \
+	  rm -rf */RCS */*~; \
+	  cd "$(TMPDIR)"; \
+	  tar cf - "$${target}" | bzip2 -9 > "$${archive}"; \
+	  cd "$${olddir}"; \
+	  rm -rf "$${tmpdir}"; \
+	  echo "archive is: $${archive}"; \
+	fi
--- /dev/null
+++ b/config.h
@@ -0,0 +1,101 @@
+/*
+ * config.h -
+ *
+ * Configuration settings for compiling Yeti and others Yorick extensions.
+ *
+ *-----------------------------------------------------------------------------
+ *
+ * Copyright (C) 1996-2010 Eric Thi�baut <thiebaut@obs.univ-lyon1.fr>
+ *
+ * This software is governed by the CeCILL-C license under French law and
+ * abiding by the rules of distribution of free software.  You can use, modify
+ * and/or redistribute the software under the terms of the CeCILL-C license as
+ * circulated by CEA, CNRS and INRIA at the following URL
+ * "http://www.cecill.info".
+ *
+ * As a counterpart to the access to the source code and rights to copy,
+ * modify and redistribute granted by the license, users are provided only
+ * with a limited warranty and the software's author, the holder of the
+ * economic rights, and the successive licensors have only limited liability.
+ *
+ * In this respect, the user's attention is drawn to the risks associated with
+ * loading, using, modifying and/or developing or reproducing the software by
+ * the user in light of its specific status of free software, that may mean
+ * that it is complicated to manipulate, and that also therefore means that it
+ * is reserved for developers and experienced professionals having in-depth
+ * computer knowledge. Users are therefore encouraged to load and test the
+ * software's suitability as regards their requirements in conditions enabling
+ * the security of their systems and/or data to be ensured and, more
+ * generally, to use and operate it in the same conditions as regards
+ * security.
+ *
+ * The fact that you are presently reading this means that you have had
+ * knowledge of the CeCILL-C license and that you accept its terms.
+ *
+ *-----------------------------------------------------------------------------
+ *
+ * $Id$
+ * $Log$
+ */
+
+#ifndef _YETI_CONFIG_H
+#define _YETI_CONFIG_H 1
+
+/*---------------------------------------------------------------------------*/
+/* Yorick version numbers: */
+#define YORICK_VERSION_MAJOR 2
+#define YORICK_VERSION_MINOR 2
+#define YORICK_VERSION_MICRO 3
+#define YORICK_VERSION_SUFFIX ""
+
+/* Yeti version numbers: */
+#define YETI_VERSION_MAJOR 6
+#define YETI_VERSION_MINOR 4
+#define YETI_VERSION_MICRO 0
+#define YETI_VERSION_SUFFIX ""
+
+/* Define the following macro to true if Yorick does not export
+   the definitions of autoload_t structure: */
+#define YETI_MUST_DEFINE_AUTOLOAD_TYPE 0
+
+/*---------------------------------------------------------------------------*/
+/* Byte order (+1 for big endian, -1 for little endian). */
+#define YETI_BYTE_ORDER -1
+
+/* Size (in bytes) of basic C-types. */
+#define YETI_CHAR_SIZE 1
+#define YETI_SHORT_SIZE 2
+#define YETI_INT_SIZE 4
+#define YETI_LONG_SIZE 8
+#define YETI_FLOAT_SIZE 4
+#define YETI_DOUBLE_SIZE 8
+#define YETI_POINTER_SIZE 8
+
+/*---------------------------------------------------------------------------*/
+/* Unfortunately the code interface to Yorick change with the version and
+   none of the Yorick headers provide this information.  The following
+   defintions attempt to provide a more uniform interface. */
+
+#undef p_strfree
+#if (YORICK_VERSION_MAJOR == 1 && YORICK_VERSION_MINOR == 4)
+# include "defstr.h"
+# define p_malloc   Ymalloc  /* usage: p_malloc(SIZE)        */
+# define p_realloc  Yrealloc /* usage: p_realloc(ADDR, SIZE) */
+# define p_free     Yfree    /* usage: p_free(ADDR)          */
+# define p_strcpy   StrCpy   /* usage: p_strcpy(STR) -- also see yeti_strcpy */
+# define p_strfree  StrFree  /* usage: p_strfree(STR) */
+# define p_stralloc StrAlloc /* usage: p_stralloc(LEN) */
+#endif /* Yorick 1.4 */
+
+#if ((YORICK_VERSION_MAJOR == 1 && YORICK_VERSION_MINOR >= 5) || YORICK_VERSION_MAJOR >= 2)
+# include "pstdlib.h"
+# define p_strfree       p_free            /* usage: p_strfree(STR) */
+# define p_stralloc(LEN) p_malloc((LEN)+1) /* usage: p_stralloc(LEN) */
+#endif /* Yorick 1.5 and newer */
+
+#ifndef p_stralloc
+# error "unsupported Yorick version"
+#endif
+
+/*---------------------------------------------------------------------------*/
+#endif /* _YETI_CONFIG_H */
--- a/core/Makefile
+++ b/core/Makefile
@@ -34,11 +34,11 @@
 #------------------------------------------------------------------------------
 
 # these values filled in by: yorick -batch make.i
-Y_MAKEDIR=
-Y_EXE=
+Y_MAKEDIR=/usr/lib/yorick
+Y_EXE=/usr/lib/yorick/bin/yorick
 Y_EXE_PKGS=
-Y_EXE_HOME=
-Y_EXE_SITE=
+Y_EXE_HOME=/usr/lib/yorick
+Y_EXE_SITE=/usr/lib/yorick
 Y_HOME_PKG=
 
 #----------------------------------------------------------- optimization flags
--- a/doc/Makefile
+++ b/doc/Makefile
@@ -1,11 +1,12 @@
 .SUFFIXES: .c .i .o .h .so .dll .dyn .a .exe .txt .text .ps .pdf .html
 #------------------------------------------------------------------------------
 # these values filled in by: yorick -batch make.i
-Y_MAKEDIR=
-Y_EXE=
+Y_MAKEDIR=/usr/lib/yorick
+Y_EXE=/usr/lib/yorick/bin/yorick
 Y_EXE_PKGS=
-Y_EXE_HOME=
-Y_EXE_SITE=
+Y_EXE_HOME=/usr/lib/yorick
+Y_EXE_SITE=/usr/lib/yorick
+Y_HOME_PKG=
 
 DOCDIR=$(Y_EXE_SITE)/doc
 
--- a/fftw/Makefile
+++ b/fftw/Makefile
@@ -1,9 +1,10 @@
 # these values filled in by    yorick -batch make.i
-Y_MAKEDIR=
-Y_EXE=
+Y_MAKEDIR=/usr/lib/yorick
+Y_EXE=/usr/lib/yorick/bin/yorick
 Y_EXE_PKGS=
-Y_EXE_HOME=
-Y_EXE_SITE=
+Y_EXE_HOME=/usr/lib/yorick
+Y_EXE_SITE=/usr/lib/yorick
+Y_HOME_PKG=
 
 # ----------------------------------------------------- optimization flags
 
@@ -22,9 +23,9 @@
 PKG_EXENAME=yorick
 
 # PKG_DEPLIBS=-Lsomedir -lsomelib   for dependencies of this package
-PKG_DEPLIBS = -L/home/soft/local/lib -lrfftw -lfftw
+PKG_DEPLIBS = -lrfftw -lfftw
 # set compiler (or rarely loader) flags specific to this package
-PKG_CFLAGS = -I/home/soft/local/include -DHAVE_FFTW=1
+PKG_CFLAGS = -DHAVE_FFTW=1
 PKG_LDFLAGS=
 
 # list of additional package names you want in PKG_EXENAME
--- a/regex/Makefile
+++ b/regex/Makefile
@@ -1,9 +1,10 @@
 # these values filled in by    yorick -batch make.i
-Y_MAKEDIR=
-Y_EXE=
+Y_MAKEDIR=/usr/lib/yorick
+Y_EXE=/usr/lib/yorick/bin/yorick
 Y_EXE_PKGS=
-Y_EXE_HOME=
-Y_EXE_SITE=
+Y_EXE_HOME=/usr/lib/yorick
+Y_EXE_SITE=/usr/lib/yorick
+Y_HOME_PKG=
 
 # ----------------------------------------------------- optimization flags
 
@@ -22,9 +23,9 @@
 PKG_EXENAME=yorick
 
 # PKG_DEPLIBS=-Lsomedir -lsomelib   for dependencies of this package
-PKG_DEPLIBS =
+PKG_DEPLIBS = 
 # set compiler (or rarely loader) flags specific to this package
-PKG_CFLAGS =
+PKG_CFLAGS = 
 PKG_LDFLAGS=
 
 # list of additional package names you want in PKG_EXENAME
--- a/tiff/Makefile
+++ b/tiff/Makefile
@@ -1,9 +1,10 @@
 # these values filled in by    yorick -batch make.i
-Y_MAKEDIR=
-Y_EXE=
+Y_MAKEDIR=/usr/lib/yorick
+Y_EXE=/usr/lib/yorick/bin/yorick
 Y_EXE_PKGS=
-Y_EXE_HOME=
-Y_EXE_SITE=
+Y_EXE_HOME=/usr/lib/yorick
+Y_EXE_SITE=/usr/lib/yorick
+Y_HOME_PKG=
 
 # ----------------------------------------------------- optimization flags
 
